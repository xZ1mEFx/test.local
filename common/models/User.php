<?php

namespace common\models;

use console\controllers\RbacController;
use Yii;
use yii\base\Exception;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\rbac\Role;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $email
 * @property string $first_name
 * @property string $last_name
 * @property float|null $balance
 * @property string $auth_key
 * @property string $password_hash
 * @property string|null $password_reset_token
 * @property int $created_at
 * @property int $updated_at
 * @property \yii\rbac\Role[] $roles
 * @property array $rolesArray
 * @property array $childrenRolesArray
 * @property boolean $youCanEdit
 * @property string $fullName
 */
class User extends ActiveRecord implements IdentityInterface
{
    const MIN_PASSWORD_LENGTH = 5;
    const MAX_PASSWORD_LENGTH = 32;

    const ROLE_ADMIN = RbacController::ROLE_ADMIN;
    const ROLE_USER = RbacController::ROLE_USER;

    protected $rolesArray;
    protected $childrenRolesArray;
    protected $youCanEdit;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = NULL)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * @return array
     */
    public static function availableRolesLabels()
    {
        $res = [];
        if (Yii::$app->user->isGuest) {
            return $res;
        }
        foreach (self::rolesLabels() as $role => $label) {
            if (Yii::$app->user->can($role)) {
                $res[$role] = $label;
            }
        }
        return $res;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @return bool
     */
    public function getYouCanEdit()
    {
        if (!isset($this->youCanEdit)) {
            $this->youCanEdit = true;
            if (!Yii::$app->user->isGuest) {
                foreach ($this->roles as $role) {
                    if (!Yii::$app->user->can($role->name)) {
                        $this->youCanEdit = false;
                        break;
                    }
                }
            }
        }
        return $this->youCanEdit;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateStatusAvailability($attribute, $params)
    {
        foreach ($this->rolesArray as $role) {
            if (!Yii::$app->user->can($role)) {
                $this->addError($attribute, 'You can not assign this roles');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'first_name', 'last_name'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['email', 'first_name', 'last_name', 'password_hash'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['auth_key'], 'default', 'value' => Yii::$app->security->generateRandomString()],
            [['email'], 'unique'],
            // virtual
            [['rolesArray'], 'safe'],
            [['rolesArray'], 'validateStatusAvailability'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'balance' => 'Balance',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            // virtual
            'roles' => 'Roles',
            'rolesArray' => 'Roles',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (!(empty($this->rolesArray) && is_array($this->rolesArray))) {
            $this->rolesArray = [$this->rolesArray];
        }
        foreach ($this->attributes as $key => $value) {
            if (empty($value)) {
                $this->setAttribute($key, NULL);
            }
        }
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        Yii::$app->authManager->revokeAll($this->id);
        parent::afterDelete();
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        $this->refreshRolesArray();
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * Generates "remember me" authentication key
     *
     * @throws Exception
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * @throws \Exception
     */
    protected function refreshRolesArray()
    {
        if (is_array($this->rolesArray)) {
            // Add new roles
            foreach ($this->rolesArray as $role) {
                if (isset($this->roles[$role])) {
                    continue;
                }
                Yii::$app->authManager->assign(Yii::$app->authManager->getRole($role), $this->id);
            }
            // Remove other roles
            foreach ($this->roles as $role) {
                if (in_array($role->name, $this->rolesArray)) {
                    continue;
                }
                Yii::$app->authManager->revoke($role, $this->id);
            }
        }
    }

    /**
     * @return array
     */
    public function getChildrenRolesArray()
    {
        if (isset($this->childrenRolesArray)) {
            return $this->childrenRolesArray;
        }
        $this->childrenRolesArray = [];
        foreach (array_keys(self::rolesLabels()) as $role) {
            if (!in_array($role, $this->getRolesArray()) && Yii::$app->authManager->checkAccess($this->id, $role)) {
                $this->childrenRolesArray[] = $role;
            }
        }
        return $this->childrenRolesArray;
    }

    /**
     * @param null $role
     * @return array|string
     */
    public static function rolesLabels($role = NULL)
    {
        $roles = [
            self::ROLE_ADMIN => 'Admin',
            self::ROLE_USER => 'User',
        ];
        if ($role === NULL) {
            return $roles;
        }

        return isset($roles[$role]) ? $roles[$role] : '';
    }

    /**
     * @return array
     */
    public function getRolesArray()
    {
        if (isset($this->rolesArray)) {
            return $this->rolesArray;
        }
        return $this->rolesArray = array_values(ArrayHelper::map($this->getRoles(), 'name', 'name'));
    }

    /**
     * @param array $value
     */
    public function setRolesArray($value)
    {
        $this->rolesArray = $value;
    }

    /**
     * @return Role[]
     */
    public function getRoles()
    {
        return Yii::$app->authManager->getRolesByUser($this->id);
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param $password
     * @throws Exception
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthAssignments()
    {
        return $this->hasMany(AuthAssignment::class, ['user_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

}
