<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "transaction_item".
 *
 * @property int $id
 * @property int $transaction_id
 * @property int $user_id
 * @property int|null $type
 * @property float|null $amount
 * @property int $created_at
 * @property int $updated_at
 */
class TransactionItem extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transaction_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['transaction_id', 'user_id', 'created_at', 'updated_at'], 'required'],
            [['transaction_id', 'user_id', 'type', 'created_at', 'updated_at'], 'integer'],
            [['amount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'transaction_id' => 'Transaction ID',
            'user_id' => 'User ID',
            'type' => 'Type',
            'amount' => 'Amount',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
