<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "transaction".
 *
 * @property int $id
 * @property int $type
 * @property int|null $cancelled
 * @property int $created_at
 * @property int $updated_at
 */
class Transaction extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transaction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'created_at', 'updated_at'], 'required'],
            [['type', 'cancelled', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'cancelled' => 'Cancelled',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
