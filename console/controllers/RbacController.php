<?php

namespace console\controllers;

use yii\console\Controller;
use yii\rbac\DbManager;

/**
 * RBAC console controller.
 */
class RbacController extends Controller
{

    const ROLE_ADMIN = 'admin';
    const ROLE_USER = 'user';

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Initial RBAC action
     */
    public function actionInit()
    {
        self::initRbac();
    }

    public static function initRbac()
    {
        $auth = new DbManager;
        $auth->init();

        $auth->removeAll();

        /*
         * Permissions
         */

        $manager = $auth->createRole(self::ROLE_USER);
        $manager->description = ucfirst(self::ROLE_USER);
        $auth->add($manager);

        $admin = $auth->createRole(self::ROLE_ADMIN);
        $admin->description = ucfirst(self::ROLE_ADMIN);
        $auth->add($admin);
        $auth->addChild($admin, $manager);
    }

}
