<?php

use console\controllers\RbacController;
use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

            // -------------------------------------------
            // Create RBAC tables
            // -------------------------------------------

            $authManager = $this->getAuthManager();

            $this->createTable($authManager->ruleTable, [
                'name' => $this->string(64)->notNull(),
                'data' => $this->text(),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
                'PRIMARY KEY (name)',
            ], $tableOptions);

            $this->createTable($authManager->itemTable, [
                'name' => $this->string(64)->notNull(),
                'type' => $this->integer()->notNull(),
                'description' => $this->text(),
                'rule_name' => $this->string(64),
                'data' => $this->text(),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
                'PRIMARY KEY (name)',
                'FOREIGN KEY (rule_name) REFERENCES ' . $authManager->ruleTable . ' (name) ON DELETE SET NULL ON UPDATE CASCADE',
            ], $tableOptions);


            $this->createIndex('idx-auth_item-type', $authManager->itemTable, 'type');

            $this->createTable($authManager->itemChildTable, [
                'parent' => $this->string(64)->notNull(),
                'child' => $this->string(64)->notNull(),
                'PRIMARY KEY (parent, child)',
                'FOREIGN KEY (parent) REFERENCES ' . $authManager->itemTable . ' (name) ON DELETE CASCADE ON UPDATE CASCADE',
                'FOREIGN KEY (child) REFERENCES ' . $authManager->itemTable . ' (name) ON DELETE CASCADE ON UPDATE CASCADE',
            ], $tableOptions);

            $this->createTable($authManager->assignmentTable, [
                'item_name' => $this->string(64)->notNull(),
                'user_id' => $this->string(64)->notNull(),
                'created_at' => $this->integer(),
                'PRIMARY KEY (item_name, user_id)',
                'FOREIGN KEY (item_name) REFERENCES ' . $authManager->itemTable . ' (name) ON DELETE CASCADE ON UPDATE CASCADE',
            ], $tableOptions);

            // Init rbac
            RbacController::initRbac();

            // -------------------------------------------
            // Create users table
            // -------------------------------------------

            $this->createTable('{{%user}}', [
                'id' => $this->primaryKey(),

                'email' => $this->string()->notNull()->unique(),
                'first_name' => $this->string()->notNull(),
                'last_name' => $this->string()->notNull(),
                'balance' => $this->double()->defaultValue(0),

                'auth_key' => $this->string(32)->notNull(),
                'password_hash' => $this->string()->notNull(),

                'created_at' => $this->integer()->notNull(),
                'updated_at' => $this->integer()->notNull(),
            ], $tableOptions);

            // -------------------------------------------
            // Insert users
            // -------------------------------------------

            // Add admin
            $this->insert('{{%user}}', [
                'auth_key' => Yii::$app->security->generateRandomString(),
                'password_hash' => Yii::$app->security->generatePasswordHash('admin'),
                'email' => 'admin@example.com',
                'first_name' => 'Administrator',
                'last_name' => 'Administrator',
//                'balance' => 0,
                'created_at' => time(),
                'updated_at' => time(),
            ]);
            $authManager->assign($authManager->getRole(RbacController::ROLE_ADMIN), 1);

            // Add user 1
            $this->insert('{{%user}}', [
                'auth_key' => Yii::$app->security->generateRandomString(),
                'password_hash' => Yii::$app->security->generatePasswordHash('user'),
                'email' => 'user1@example.com',
                'first_name' => 'User 1',
                'last_name' => 'User 1',
                'balance' => 555.77,
                'created_at' => time(),
                'updated_at' => time(),
            ]);
            $authManager->assign($authManager->getRole(RbacController::ROLE_USER), 2);

            // Add user 2
            $this->insert('{{%user}}', [
                'auth_key' => Yii::$app->security->generateRandomString(),
                'password_hash' => Yii::$app->security->generatePasswordHash('user'),
                'email' => 'user2@example.com',
                'first_name' => 'User 2',
                'last_name' => 'User 2',
//                'balance' => 0,
                'created_at' => time(),
                'updated_at' => time(),
            ]);
            $authManager->assign($authManager->getRole(RbacController::ROLE_USER), 3);

            // Add user 3
            $this->insert('{{%user}}', [
                'auth_key' => Yii::$app->security->generateRandomString(),
                'password_hash' => Yii::$app->security->generatePasswordHash('user'),
                'email' => 'user3@example.com',
                'first_name' => 'User 3',
                'last_name' => 'User 3',
                'balance' => 1567.3,
                'created_at' => time(),
                'updated_at' => time(),
            ]);
            $authManager->assign($authManager->getRole(RbacController::ROLE_USER), 4);

            // -------------------------------------------
            // Create transactions tables
            // -------------------------------------------

            $this->createTable('{{%transaction}}', [
                'id' => $this->primaryKey(),

                'type' => $this->smallInteger(1)->notNull(),
                'cancelled' => $this->smallInteger(1)->defaultValue(0),

                'created_at' => $this->integer()->notNull(),
                'updated_at' => $this->integer()->notNull(),
            ], $tableOptions);

            $this->createTable('{{%transaction_item}}', [
                'id' => $this->primaryKey(),
                'transaction_id' => $this->integer(11)->notNull(),
                'user_id' => $this->integer(11)->notNull(),

                'type' => $this->smallInteger(1),
                'amount' => $this->double()->defaultValue(0),

                'created_at' => $this->integer()->notNull(),
                'updated_at' => $this->integer()->notNull(),
            ], $tableOptions);

            $this->createIndex('transaction_id', '{{%transaction_item}}', 'transaction_id');
            $this->addForeignKey('transaction_id_fk', '{{%transaction_item}}', 'transaction_id', '{{%transaction}}', 'id', 'RESTRICT', 'RESTRICT');
            $this->createIndex('user_id', '{{%transaction_item}}', 'user_id');
            $this->addForeignKey('user_id_fk', '{{%transaction_item}}', 'user_id', '{{%user}}', 'id', 'RESTRICT', 'RESTRICT');
        }
    }

    protected function getAuthManager()
    {
        return new \yii\rbac\DbManager();
    }

    public function down()
    {
        $this->dropTable('{{%transaction_item}}');
        $this->dropTable('{{%transaction}}');
        $this->dropTable('{{%user}}');

        $authManager = $this->getAuthManager();
        $this->dropTable($authManager->assignmentTable);
        $this->dropTable($authManager->itemChildTable);
        $this->dropTable($authManager->itemTable);
        $this->dropTable($authManager->ruleTable);
    }
}
