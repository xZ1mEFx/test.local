<?php

namespace backend\models\forms;

use common\models\User;
use yii\base\Exception;
use yii\base\Model;

/**
 * UserSearch represents the model behind the search form about `common\models\User`.
 *
 * @property string $newPassword
 * @property string $newPasswordConfirm
 * @property User $_user
 */
class ChangeUserPasswordForm extends Model
{

    public $newPassword;
    public $newPasswordConfirm;
    private $_user;

    public function __construct($id, $config = [])
    {
        $this->_user = User::findIdentity($id);
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['newPassword', 'string', 'min' => User::MIN_PASSWORD_LENGTH, 'max' => User::MAX_PASSWORD_LENGTH],
            ['newPassword', 'filter', 'filter' => 'trim'],
            ['newPassword', 'required'],
            ['newPasswordConfirm', 'required'],
            ['newPasswordConfirm', 'compare', 'compareAttribute' => 'newPassword', 'message' => 'Passwords are not equal'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'newPassword' => 'Password',
            'newPasswordConfirm' => 'Password confirm',
        ];
    }

    /**
     * Change user password
     *
     * @return bool
     * @throws Exception
     */
    public function change()
    {
        if ($this->validate()) {
            $this->_user->setPassword($this->newPassword);
            return $this->_user->save(false);
        }
        return false;
    }

}
