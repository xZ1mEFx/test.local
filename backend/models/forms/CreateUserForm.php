<?php

namespace backend\models\forms;

use common\models\User;
use yii\helpers\ArrayHelper;

/**
 * @inheritdoc
 *
 * @property string $newPassword
 * @property string $newPasswordConfirm
 */
class CreateUserForm extends User
{

    public $newPassword;
    public $newPasswordConfirm;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['balance'], 'number'],
            [['balance'], 'default', 'value' => 0],
            ['newPassword', 'string', 'min' => self::MIN_PASSWORD_LENGTH, 'max' => self::MAX_PASSWORD_LENGTH],
            ['newPassword', 'filter', 'filter' => 'trim'],
            ['newPassword', 'required'],
            ['newPasswordConfirm', 'required'],
            ['newPasswordConfirm', 'compare', 'compareAttribute' => 'newPassword', 'message' => 'Passwords are not equal'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'newPassword' => 'Password',
            'newPasswordConfirm' => 'Password confirm',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if (!empty($this->newPassword) && empty($this->password_hash)) {
                $this->setPassword($this->newPassword);
            }
            return true;
        }
        return false;
    }

}
