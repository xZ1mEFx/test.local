<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model LoginForm */

use common\models\LoginForm;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$this->title = 'Login';
?>

<div class="row">
    <div class="col-md-offset-3 col-md-6">
        <div class="well">
            <?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => [
                'class' => '',
            ]]); ?>

            <h1><?= Html::encode($this->title) ?></h1>

            <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <?= $form->field($model, 'rememberMe')->checkbox() ?>

            <div class="form-group">
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
