<?php

use common\models\User;
use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model User */

$this->title = $model->fullName;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);

$userRoles = '';
foreach ($model->rolesArray as $role) {
    $userRoles .= empty($userRoles) ? '' : '&nbsp;';
    $userRoles .= Html::tag('span', User::rolesLabels($role), ['class' => 'label label-primary']);
}
foreach ($model->childrenRolesArray as $role) {
    $userRoles .= empty($userRoles) ? '' : '&nbsp;';
    $userRoles .= Html::tag('span', User::rolesLabels($role), ['class' => 'label label-info', 'title' => 'Унаследованное право']);
}
?>
<div class="transaction-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if ($model->youCanEdit): ?>
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?php if ($model->id !== (int)Yii::$app->user->id): ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            <?php endif; ?>
        <?php endif; ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'email:email',
            'first_name',
            'last_name',
            'balance',
            'email:email',
            [
                'label' => 'Roles',
                'attribute' => 'role',
                'format' => 'raw',
                'value' => $userRoles,
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
