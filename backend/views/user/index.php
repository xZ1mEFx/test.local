<?php

use common\models\User;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => SerialColumn::class],

//            'id',
            'email:email',
            'first_name',
            'last_name',
            [
                'attribute' => 'balance',
                'filter' => false,
            ],
            [
                'attribute' => 'roles',
                'filter' => false,
                'headerOptions' => ['class' => 'col-lg-2'],
                'contentOptions' => ['class' => 'text-center col-lg-2'],
                'content' => function ($model) {
                    /* @var $model User */
                    $res = '';
                    foreach ($model->rolesArray as $role) {
                        $res .= empty($res) ? '' : '&nbsp;';
                        $res .= Html::tag('span', User::rolesLabels($role), ['class' => 'label label-primary']);
                    }
                    foreach ($model->childrenRolesArray as $role) {
                        $res .= empty($res) ? '' : '&nbsp;';
                        $res .= Html::tag('span', User::rolesLabels($role), ['class' => 'label label-info', 'title' => 'Inherited role']);
                    }
                    return $res;
                },
            ],
            [
                'attribute' => 'created_at',
                'filter' => false,
                'format' => 'datetime',
            ],
            [
                'attribute' => 'updated_at',
                'filter' => false,
                'format' => 'datetime',
            ],

            [
                'class' => ActionColumn::class,
                'contentOptions' => ['class' => 'text-center'],
                'template' => '{plus} {retweet}<hr style="margin: 0;">{view} {update} {delete}',
                'buttons' => [
                    'plus' => function ($model, $key, $index) {
                        return Html::a('<span class="glyphicon glyphicon-plus"></span>', '#', [
                            'title' => Yii::t('yii', 'Add money'),
                        ]);
                    },
                    'retweet' => function ($model, $key, $index) {
                        return Html::a('<span class="glyphicon glyphicon-retweet"></span>', '#', [
                            'title' => Yii::t('yii', 'Send money'),
                        ]);
                    },
                ],
                'visibleButtons' => [
                    'update' => function ($model, $key, $index) {
                        /* @var $model User */
                        return $model->youCanEdit;
                    },
                    'delete' => function ($model, $key, $index) {
                        /* @var $model User */
                        return $model->youCanEdit && $model->id !== Yii::$app->user->id;
                    },
                ],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
