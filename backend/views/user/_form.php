<?php

use common\models\User;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
/* @var $changePasswordModel backend\models\forms\ChangeUserPasswordForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(['enableAjaxValidation' => true, 'validateOnType' => true]); ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

    <?php if ($model->isNewRecord): ?>
        <?= $form->field($model, 'balance')->textInput() ?>
    <?php endif; ?>

    <br>

    <?php if ($model->isNewRecord): ?>
        <?= $form->field($model, 'newPassword')->passwordInput() ?>

        <?= $form->field($model, 'newPasswordConfirm')->passwordInput() ?>

        <br>
    <?php endif; ?>

    <?= $form->field($model, 'rolesArray')->radioList(User::availableRolesLabels(), ['separator' => '<br />']) ?>

    <div class="form-group">
        <?php if (!$model->isNewRecord) : ?>
            <?= Html::a('Change password', '#cp-modal', ['class' => 'btn btn-danger', 'data-toggle' => 'modal',]) ?>
        <?php endif; ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Change', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php if (!$model->isNewRecord) : ?>
    <div class="row">
        <?php
        Modal::begin([
            'id' => 'cp-modal',
            'header' => '<b>Change password</b>',
            'clientOptions' => false,
        ]);
        ?>
        <?php $form = ActiveForm::begin(['action' => Url::toRoute(['user/update-password/', 'id' => $model->id]), 'options' => ['role' => 'form']]); ?>
        <?= $form->field($changePasswordModel, 'newPassword')->passwordInput() ?>
        <?= $form->field($changePasswordModel, 'newPasswordConfirm')->passwordInput() ?>
        <div class="form-group">
            <?= Html::submitButton('Change', ['class' => 'btn btn-danger']) ?>
        </div>
        <?php ActiveForm::end(); ?>
        <?php Modal::end(); ?>
    </div>
<?php endif; ?>
