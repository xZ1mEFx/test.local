<?php

namespace backend\controllers;

use backend\models\forms\ChangeUserPasswordForm;
use backend\models\forms\CreateUserForm;
use backend\models\search\UserSearch;
use common\models\User;
use Throwable;
use Yii;
use yii\base\Exception;
use yii\bootstrap\ActiveForm;
use yii\db\StaleObjectException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['GET'],
                    'view' => ['GET'],
                    'create' => ['GET', 'POST'],
                    'update' => ['GET', 'PUT', 'POST'],
                    'update-password' => ['PUT', 'POST'],
                    'delete' => ['POST', 'DELETE'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @return array|string|Response
     */
    public function actionCreate()
    {
        $model = new CreateUserForm();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            print_r($model->errors);
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @param $id
     * @return array|string|Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        // Check rights
        foreach ($model->rolesArray as $role) {
            if (!Yii::$app->user->can($role)) {
                throw new ForbiddenHttpException("You have no enough rights");
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'changePasswordModel' => new ChangeUserPasswordForm($id),
            ]);
        }
    }

    /**
     * @param $id
     * @return Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        if ((int)$id == (int)Yii::$app->user->id) {
            Yii::$app->session->setFlash('error', 'You can\'t delete yourself!');
            return $this->redirect(['index']);
        }

        $model = $this->findModel($id);

        // Check rights
        foreach ($model->rolesArray as $role) {
            if (!Yii::$app->user->can($role)) {
                throw new ForbiddenHttpException("You have no enough rights");
            }
        }

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws Exception
     */
    public function actionUpdatePassword($id)
    {
        $model = new ChangeUserPasswordForm($id);

        // Check rights
        foreach ($this->findModel($id)->rolesArray as $role) {
            if (!Yii::$app->user->can($role)) {
                throw new ForbiddenHttpException("You have no enough rights");
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->change()) {
            Yii::$app->session->setFlash('success', 'Password successfully changed');
        } else {
            $errors = '';
            foreach ($model->errors as $field) {
                foreach ($field as $error) {
                    $errors .= empty($errors) ? '' : '<br>';
                    $errors .= $error;
                }
            }
            if (!empty($errors)) {
                Yii::$app->session->setFlash('danger', "Password hasn't changed!<br>$errors");
            }
        }

        return $this->redirect(['update', 'id' => $id]);
    }
}
